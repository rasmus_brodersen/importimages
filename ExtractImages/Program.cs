﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using Dapper;
using System.Drawing;

namespace ImportImages {
    class Program {
        static void Main(string[] args) {

            SqlConnection connection = new SqlConnection(@"Data Source=sqlpmc.mssql.gdev.local;Initial Catalog=S_Linkoping_190425_jgn;Integrated Security=SSPI;Persist Security Info=False");
            connection.Open();

            var importPath = @"C:\Temp\Resized";

            string[] pictures = Directory.GetFiles(importPath);
            int featID = 500;
            foreach (string picture in pictures) {
                var convertedImage = new System.Drawing.Bitmap(picture);
                Image thumb = (Image)(new Bitmap(convertedImage, new Size(convertedImage.Width / 10, convertedImage.Height / 10)));   //.GetThumbnailImage(32, 32, null, System.IntPtr.Zero);
                byte[] photoReduced = null;
                using (var ms = new MemoryStream()) {
                    thumb.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                    photoReduced = ms.ToArray();
                }

                FileStream fs = new FileStream(picture, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                byte[] photoOriginal = br.ReadBytes((int)fs.Length);
                br.Close();
                fs.Close();
                //var convertedImage = new System.Drawing.Bitmap(picture);
                int fileNameStart = picture.LastIndexOf('\\') + 1;
                int strPos = picture.LastIndexOf(".");

                ////Insert new signs
                //if (2 > 1) {
                //    string pictureName1 = picture.Substring(fileNameStart, 1).ToUpper() + picture.Substring(fileNameStart + 1, strPos - fileNameStart - 1); //" " +
                //    featID = featID + 1;
                //    int res1 = connection.Execute("INSERT INTO biroFeatureObjects (ID, FOType, Sort, FOName) VALUES (" + featID.ToString() + ", 0, " + featID.ToString() + ", '" + pictureName1 + "')");
                //    continue;
                //}

                string pictureName = picture.Substring(fileNameStart, 1).ToUpper() + picture.Substring(fileNameStart + 1, strPos - fileNameStart - 1) + "%"; //" " +
                //if (pictureName.StartsWith("S") || pictureName.StartsWith("T"))  photoReduced = photoOriginal;

                List<FeatObj> feats = connection.Query<FeatObj>("Select FOName, Picture from biroFeatureObjects where FOName like '" + pictureName + "' and FOType = 0").ToList();
                if (feats.Count == 0) continue;
                if (feats[0].Picture != null) continue;
 
                int res = connection.Execute("update biroFeatureObjects set picture = @val where FOName like @id and FOType = 0", new { val = photoReduced, id = pictureName });
                if (res == 0) {
                    strPos = picture.LastIndexOf("-");
                    if (strPos == -1) strPos = picture.LastIndexOf(".");
                    pictureName = picture.Substring(fileNameStart, 1).ToUpper() + " " + picture.Substring(fileNameStart + 1, strPos - fileNameStart - 1) + " %";
                    res = connection.Execute("update biroFeatureObjects set picture = @val where FOName like @id and FOType = 0", new { val = photoReduced, id = pictureName });
                }
            }


            //SqlConnection connection = new SqlConnection(@"Data Source=DKCPW00365;Initial Catalog=Frederikshavn_GIS;Integrated Security=SSPI;Persist Security Info=False;");
            //SqlConnection connection = new SqlConnection(@"Data Source=10.1.13.13\Germany;Initial Catalog=RoSy_Mora;Persist Security Info=True;User ID=rbr1;Password= ");


            //connection.Open();

            //    List<FeatObj> feats = connection.Query<FeatObj>("Select FOName, Picture from biroFeatureObjects where Picture IS NOT NULL").ToList();

            //    var exportPath = @"R:\Groups\KOL\pmc\_Staff\ADO\Skyltar Rosy";
            //    if (!Directory.Exists(exportPath)) {
            //        Directory.CreateDirectory(exportPath);
            //    }
            //    foreach (FeatObj row in feats) {
            //        var imageBytes = row.Picture;
            //        if (imageBytes.Length > 0) {
            //            using (var convertedImage = new System.Drawing.Bitmap(new MemoryStream(imageBytes))) {
            //                int BlankOrLast = row.FOName.IndexOf(" ");
            //                if (BlankOrLast == -1) BlankOrLast = row.FOName.Length; 
            //                string fname = row.FOName.Substring(0, BlankOrLast);
            //                fname = fname.Replace("/","_");
            //                var fileName = Path.Combine(exportPath,  fname + ".bmp");
            //                if (File.Exists(fileName)) {
            //                    File.Delete(fileName);
            //                }
            //                convertedImage.Save(fileName);
            //            }
            //        }
            //    }
        }
    }
}